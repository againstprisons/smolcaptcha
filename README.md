# SmolCaptcha

A small CAPTCHA render/verification server.

Generated CAPTCHAs are simple math questions:
addition or subtraction of two single-digit numbers.

## Usage

TODO

## License

SmolCaptcha is licensed under the MIT License,
see [the LICENSE file](./LICENSE) for details.
