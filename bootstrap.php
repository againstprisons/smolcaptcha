<?php
declare(strict_types=1);
define('IX_ENVBASE', 'SITE');
define('IX_BASE', dirname(__FILE__));
require_once(IX_BASE . '/vendor/autoload.php');

use ix\HookMachine;
use ix\Container\Container;
use ix\Controller\Controller;
use ix\Application\Application;

/* Container hooks */
HookMachine::add([Container::class, 'construct'], '\ix\Container\ContainerHooksHtmlRenderer::hookContainerHtmlRenderer');

/* Application routes */
HookMachine::add([Application::class, 'create_app', 'routeRegister'], (function ($key, $app) {
	$app->post('/api/generate', \SmolCaptcha\Controllers\ApiGenerateController::class)->setName('api-generate');
	$app->post('/api/verify', \SmolCaptcha\Controllers\ApiVerifyController::class)->setName('api-verify');
	$app->get('/render/{token}', \SmolCaptcha\Controllers\CaptchaRenderController::class)->setName('render');

	return $app;
}));
