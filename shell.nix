{ pkgs ? import <nixpkgs> {}, lib ? pkgs.lib }:

with lib;

let
  neededLibraries = with pkgs; [
    curl
  ];

  ourPhp80 = pkgs.php80.withExtensions ({ all, enabled, ... }: with all; [
    curl
    redis
    imagick
  ] ++ enabled);

in
pkgs.mkShell {
  buildInputs = with pkgs; [
    ourPhp80
    ourPhp80.packages.composer

    cacert
  ] ++ neededLibraries;

  "LD_LIBRARY_PATH" = makeLibraryPath neededLibraries;
  "SSL_CERT_DIR" = "${pkgs.cacert}/etc/ssl/certs";
  "SSL_CERT_FILE" = "${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt";
}
