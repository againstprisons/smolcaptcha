<?php
declare(strict_types=1);
namespace SmolCaptcha;

use Dotenv\Dotenv;

class Configuration {
	/** @var ?Dotenv $dotenv */
	private static $dotenv = null;

	/**
	 * Create or return a Dotenv instance
	 *
	 * @param string $mode
	 * @return Dotenv
	 */
	public static function load(string $mode = 'base'): Dotenv {
		if (self::$dotenv === null) {
			self::$dotenv = self::create($mode);
		}

		return self::$dotenv;
	}

	/**
	 * Construct a new Dotenv instance
	 *
	 * @param string $mode
	 * @return Dotenv
	 */
	private static function create(string $mode = 'base'): Dotenv {
		// Construct and load Dotenv
		$dotenv = Dotenv::createImmutable(IX_BASE);
		$dotenv->load();

		// Require an environment, a Redis URL, and a session token name
		$dotenv->required('APP_ENV')->allowedValues(['development', 'test', 'production']);
		$dotenv->required('REDIS_URL')->notEmpty();
		$dotenv->required(IX_ENVBASE . '_SESSIONCOOKIE')->notEmpty();

		return $dotenv;
	}
}
