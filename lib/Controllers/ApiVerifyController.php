<?php
declare(strict_types=1);
namespace SmolCaptcha\Controllers;

use ix\Controller\Controller;
use ix\Database\RedisContainer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;

class ApiVerifyController extends Controller {
	/**
	 * @param Request $request The Request object
	 * @param Response $response The Response object
	 * @param mixed[] $args Arguments passed from the router (if any)
	 * @return Response The resulting Response object
	 */
	public function requestPOST(Request $request, Response $response, ?array $args = []): Response {
		$redis = RedisContainer::get();
		$bodyValues = (array) $request->getParsedBody();

		// Check for valid API token
		if (!array_key_exists('client', $bodyValues))
			throw new HttpBadRequestException($request);
		$client = trim($bodyValues['client']);
		if ($redis->exists("smolcaptcha:client:" . $client) < 1)
			throw new HttpBadRequestException($request);

		// Check for valid CAPTCHA token
		if (!array_key_exists('captcha', $bodyValues))
			throw new HttpBadRequestException($request);
		$cToken = trim($bodyValues['captcha']);
		$cRedisKey = "smolcaptcha:cdata:" . $cToken;
		if ($redis->exists($cRedisKey) < 1)
			throw new HttpBadRequestException($request);

		// Get CAPTCHA data
		$cData = $redis->hGetAll($cRedisKey);

		// Check the CAPTCHA was issued to this client
		if ($cData['client'] !== $client)
			throw new HttpBadRequestException($request);

		// Get the provided result text
		if (!array_key_exists('result', $bodyValues))
			throw new HttpBadRequestException($request);
		$cResult = trim($bodyValues['result']);

		// Wipe this CAPTCHA from Redis now that we have the data
		$redis->del($cRedisKey);

		// Check the result text
		if ($cResult === $cData['result']) {
			$response = $response->withStatus(200);
			$response->getBody()->write("ok");
			return $response;
		}

		$response = $response->withStatus(418);
		$response->getBody()->write("fail");
		return $response;
	}
}


