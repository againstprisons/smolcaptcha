<?php
declare(strict_types=1);
namespace SmolCaptcha\Controllers;

use ix\Controller\Controller;
use ix\Database\RedisContainer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;

class CaptchaRenderController extends Controller {
	/**
	 * @param Request $request The Request object
	 * @param Response $response The Response object
	 * @param mixed[] $args Arguments passed from the router (if any)
	 * @return Response The resulting Response object
	 */
	public function requestGET(Request $request, Response $response, ?array $args = []): Response {
		$redis = RedisContainer::get();
		$routeArgs = $request->getAttribute('__routingResults__')->getRouteArguments();
		if (!array_key_exists('token', $routeArgs))
			throw new HttpBadRequestException($request);

		$cdatakey = "smolcaptcha:cdata:" . trim($routeArgs['token']);
		if ($redis->exists($cdatakey) < 1)
			throw new HttpNotFoundException($request);

		// Get our data
		$cdata = $redis->hGetAll($cdatakey);

		// Construct image
		$iwidth = 280;
		$iheight = 180;
		$im = new \Imagick();
		$im->newImage($iwidth, $iheight, new \ImagickPixel('white'));

		// Draw a random ellipse on there
		$imDraw = new \ImagickDraw();

		// Set font drawing options
		$imDraw->setFillColor('#ff3333');
		$imDraw->setStrokeColor('gray40');
		$imDraw->setStrokeWidth(3);
		$imDraw->setFontSize(64);
		$imDraw->setFont('DejaVu-Sans');
		$imDraw->setGravity(\Imagick::GRAVITY_CENTER);

		// Draw our text
		$im->annotateImage($imDraw, 0, rand(-40, 40), 0, $cdata['text']);

		// Distort the text
		$im->distortImage(\Imagick::DISTORTION_ARC, [rand(5, 60), rand(0, 10)], true);

		// Resize back after distortion
		$im->resizeImage($iwidth, $iheight, \Imagick::FILTER_LANCZOS, 1.0);

		// Add some noise to the image
		$im->addNoiseImage(\Imagick::NOISE_MULTIPLICATIVEGAUSSIAN, \Imagick::CHANNEL_GRAY);
		$im->addNoiseImage(\Imagick::NOISE_GAUSSIAN);

		// We're outputting a PNG
		$im->setImageFormat('png');

		// Write image to browser
		$response = $response->withHeader('Content-Type', 'image/png');
		$response->getBody()->write((string) $im->getImageBlob());
		return $response;
	}
}
