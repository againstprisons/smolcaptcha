<?php
declare(strict_types=1);
namespace SmolCaptcha\Controllers;

use ix\Controller\Controller;
use ix\Database\RedisContainer;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;

class ApiGenerateController extends Controller {
	/**
	 * @param Request $request The Request object
	 * @param Response $response The Response object
	 * @param mixed[] $args Arguments passed from the router (if any)
	 * @return Response The resulting Response object
	 */
	public function requestPOST(Request $request, Response $response, ?array $args = []): Response {
		$redis = RedisContainer::get();
		$bodyValues = (array) $request->getParsedBody();

		// Check for valid API token
		if (!array_key_exists('client', $bodyValues))
			throw new HttpBadRequestException($request);
		$client = trim($bodyValues['client']);
		if ($redis->exists("smolcaptcha:client:" . $client) < 1)
			throw new HttpBadRequestException($request);

		// Generate a CAPTCHA token
		$cToken = bin2hex(random_bytes(32));
		$cRedisKey = "smolcaptcha:cdata:" . $cToken;

		// Generate the CAPTCHA information
		$cNumOne = rand(1, 10);
		$cNumTwo = rand(1, 10);
		$cNumResult = $cNumOne + $cNumTwo;
		$cText = "{$cNumOne} + {$cNumTwo}";

		// Store the CAPTCHA information
		$redis->hSet($cRedisKey, "client", $client);
		$redis->hSet($cRedisKey, "text", $cText);
		$redis->hSet($cRedisKey, "result", (string)$cNumResult);
		$redis->expire($cRedisKey, 60 * 30); // 30 minute key expiry

		// Return the token
		$response = $response
			->withHeader('Content-Type', 'text/plain')
			->withStatus(200);
		$response->getBody()->write($cToken);
		return $response;
	}
}

